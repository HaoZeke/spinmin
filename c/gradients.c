#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
// #define LINTP(a) ((long int*)PyArray_DATA(a))

double dot3(const double *x, const double *y);
void cross(const double *x, const double *y, double *out);
void calc_diff(double *x, double *y, double *out);
void get_offset_positions(double *x, double *offset,
                          double *cell, double *out);
void normalize(double *x);
int get_incriment(int k);


static PyObject* xgrad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    double J;

    if (!PyArg_ParseTuple(args, "DOOOO", &J, &spins, &nbl1, &nbl2, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    int k;

    int i1;
    int i2;
    int l;


    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) -=
                *(pspin + i2 * 3 + l) * J;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* dmgrad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *nndist;

    double D;
    int bloch = 0;
    PyArrayObject *NeelVec;

    if (!PyArg_ParseTuple(
            args, "DOOOOOOOOOI", &D, &spins, &nbl1, &nbl2, &nndist,
            &positions, &offsets, &cell, &grad, &NeelVec, &bloch))

        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnvec = DOUBLEP(NeelVec);
    double* pnndist = DOUBLEP(nndist);

    double cross_prod[3] = {0.0, 0.0, 0.0};
    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double dmv[3] = {0.0, 0.0, 0.0};
    double r_dist=0.0;

    int k;
    int i1;
    int i2;
    int l;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        if (fabs(pnndist[0] - r_dist) > 0.01){
            continue;
        }

        normalize(r_ij);

        if (bloch == 0)
            cross(pspin + 3 * i2, r_ij, cross_prod);
        else{
            cross(r_ij, pnvec, dmv);
            cross(pspin + 3 * i2, dmv, cross_prod);
        }

        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) -=
                cross_prod[l] * D;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* angrad(PyObject* self, PyObject* args){

    PyArrayObject *grad;
    PyArrayObject *spins;
    double K;
    PyArrayObject *e_an;

    if (!PyArg_ParseTuple(args, "DOOO", &K, &e_an, &spins, &grad))
        return NULL;

    double* pgrad = DOUBLEP(grad);
    double* pspin = DOUBLEP(spins);
    double* pe = DOUBLEP(e_an);
    double dot_prod = 0.0;
    int k;
    int l;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        dot_prod = dot3(pspin + k * 3, pe);
        for(l = 0; l < 3; l++ ){
            Py_INCREF(grad);
            *(pgrad + 3 * k + l) -= *(pe + l)* dot_prod * K * 2.0;
            Py_DECREF(grad);
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* zgrad(PyObject* self, PyObject* args){

    PyArrayObject *grad;
    double Z;
    PyArrayObject *e_b;

    if (!PyArg_ParseTuple(args, "DOO", &Z, &e_b, &grad))
        return NULL;

    double* pgrad = DOUBLEP(grad);
    double* pe = DOUBLEP(e_b);
    int k;
    int l;

    for(k = 0; k < PyArray_DIMS(grad)[0]; k++){
        for(l = 0; l < 3; l++ ){
            Py_INCREF(grad);
            *(pgrad + 3 * k + l) -= *(pe + l) * Z;
            Py_DECREF(grad);
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* xnn_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *nndist;
    PyArrayObject *nnJ;

    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *grad;

    if (!PyArg_ParseTuple(
            args, "OOOOOOOOO",
            &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell,
            &nndist, &nnJ, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pnndist = DOUBLEP(nndist);
    double* pnnJ = DOUBLEP(nnJ);
    double* pgrad = DOUBLEP(grad);

    int nshells = PyArray_DIMS(nnJ)[0];

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist = 0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me=0;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){

        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);

        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }
        for(l = 0; l < 3; l++){
            Py_INCREF(grad);
            *(pgrad + 3 * i1 + l) -=
                *(pspin + i2 * 3 + l) * pnnJ[me];
            Py_DECREF(grad);
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* i44grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    double J;

    if (!PyArg_ParseTuple(args, "DOO", &J, &spins, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

//    int k;
    int l;

    int i=0;
    int m1=0;
    int m2=0;
    int m3=0;
    int m4=0;
    double energy = 0.0;

    int n_atoms = PyArray_DIMS(spins)[0];
    int N = (int) sqrt((double) n_atoms);

    double s12=0.0;
    double s14=0.0;
    double s13=0.0;

    double s34=0.0;
    double s23=0.0;
    double s24=0.0;

    for(i = 0; i < n_atoms; i++){
        //  _
        // /_/
        if ((i+1)%N == 0 && N-1 <= i && i <= (N-1)*N-1){
            m1 = i;
            m2 = i - (N-1);
            m3 = i + N - (N-1);
            m4 = i + N;
        }
        else if (N*(N - 1) <= i && i < N*(N - 1) + N -1){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N-1)*N + 1;
            m4 = i - (N-1)*N;
        }
        else if (i == N*(N-1) + (N-1)){
            m1 = i;
            m2 = i - (N-1);
            m3 = i - (N-1) - (N*N-N);
            m4 = i - (N*N-N);
        }
        else{
            m1 = i;
            m2 = i + 1;
            m3 = i + 1 + N;
            m4 = i + N;
        }

        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        s14 = dot3(pspin + m1 * 3, pspin + 3 * m4);
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s34 = dot3(pspin + m3 * 3, pspin + 3 * m4);
        s23 = dot3(pspin + m2 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s12*s34 + s14*s23 - s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s34+
                *(pspin + m4 * 3 + l) * J * s23-
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s34+
                *(pspin + m3 * 3 + l) * J * s14-
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m4 * 3 + l) * J * s12+
                *(pspin + m2 * 3 + l) * J * s14-
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m3 * 3 + l) * J * s12+
                *(pspin + m1 * 3 + l) * J * s23-
                *(pspin + m2 * 3 + l) * J * s13;
        }
        //  _
        // \_\

        if (i % N == 0 && 0 <= i / N  && i / N <= N - 2){
            m1 = i;
            m2 = i + 1;
            m3 = i + N;
            m4 = i + N + (N - 1);
        }
        else if (N * (N - 1) + 1 <= i && i < N * (N - 1) + N - 1){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N-1)*N;
            m4 = i - (N-1)*N - 1;
        }
        else if ((i+1)%N==0 && N-1<=i && i<=(N-1)*N-1){
            m1 = i;
            m2 = i - (N - 1);
            m3 = i + N;
            m4 = i + N - 1;
        }
        else if (i==(N+1)*(N-1)){
            m1 = i;
            m2 = i - (N - 1);
            m3 = i - (N + 1)* (N - 1) + N - 1;
            m4 = i - (N + 1)* (N - 1) + N - 1 - 1;
        }
        else if (i==N*(N-1)){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N - 1) * N;
            m4 = i - (N - 1) * N + (N - 1);
        }
        else{
            m1 = i;
            m2 = i + 1;
            m3 = i + N;
            m4 = i + N - 1;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        s14 = dot3(pspin + m1 * 3, pspin + 3 * m4);
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s34 = dot3(pspin + m3 * 3, pspin + 3 * m4);
        s23 = dot3(pspin + m2 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s12*s34 + s14*s23 - s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s34+
                *(pspin + m4 * 3 + l) * J * s23-
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s34+
                *(pspin + m3 * 3 + l) * J * s14-
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m4 * 3 + l) * J * s12+
                *(pspin + m2 * 3 + l) * J * s14-
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m3 * 3 + l) * J * s12+
                *(pspin + m1 * 3 + l) * J * s23-
                *(pspin + m2 * 3 + l) * J * s13;
        }

        // /\
        // \/
        if (i % N == 0 && 0 <= i / N  && i / N <= N - 3){
            m1 = i;
            m2 = i + N;
            m3 = i + 2 * N - 1 + N;
            m4 = i + 2 * N - 1;
        }
        else if (i%N==0 && i/N==N-2){
            m1 = i;
            m2 = i + N;
            m3 = i - N * (N - 2) + N - 1;
            m4 = i + N + N - 1;
        }
        else if (i == N * (N - 1)){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N + (N - 1) + N;
            m4 = i - (N - 1) * N + (N - 1);
        }
        else if (N*(N-1)+1<=i && i<=N*(N-1)+N-1){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N + N - 1;
            m4 = i - (N - 1) * N - 1;
        }
        else if (N*(N-2)+1<=i && i<N*(N-1)+N-1){
            m1 = i;
            m2 = i + N;
            m3 = i + N - 1 - (N - 1) * N;
            m4 = i + N - 1;
        }
        else{
            m1 = i;
            m2 = i + N;
            m3 = i + 2 * N - 1;
            m4 = i + N - 1;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        s14 = dot3(pspin + m1 * 3, pspin + 3 * m4);
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s34 = dot3(pspin + m3 * 3, pspin + 3 * m4);
        s23 = dot3(pspin + m2 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s12*s34 + s14*s23 - s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s34+
                *(pspin + m4 * 3 + l) * J * s23-
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s34+
                *(pspin + m3 * 3 + l) * J * s14-
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m4 * 3 + l) * J * s12+
                *(pspin + m2 * 3 + l) * J * s14-
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m3 * 3 + l) * J * s12+
                *(pspin + m1 * 3 + l) * J * s23-
                *(pspin + m2 * 3 + l) * J * s13;
        }


    }
    energy *= -J;
    return Py_BuildValue("d", energy);
}


static PyObject* i34grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    double J;

    if (!PyArg_ParseTuple(args, "DOO", &J, &spins, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

//    int k;
    int l;

    int i=0;
    int m1=0;
    int m2=0;
    int m3=0;
    double energy = 0.0;

    int n_atoms = PyArray_DIMS(spins)[0];
    int N = (int) sqrt((double) n_atoms);

    double s12=0.0;
    double s13=0.0;
    double s23=0.0;

    for(i = 0; i < n_atoms; i++){
        //
        // /\
        // -
        if ( (i+1) % N == 0 && N - 1 <= i && i<=(N - 1)*N - 1){
            m1 = i;
            m2 = i - (N-1);
            m3 = i + N;
        }
        else if (N*(N - 1) <= i && i <= N*(N - 1) + N - 2){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N - 1)*N;
        }
        else if (i == (N + 1)*(N-1)){
            m1 = i;
            m2 = i - (N-1);
            m3 = i - (N-1) * N;
        }
        else{
            m1 = i;
            m2 = i + 1;
            m3 = i + N;
        }

        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s23 = dot3(pspin + m2 * 3, pspin + 3 * m3);
        energy += s12*s13 + s12*s23 + s13*s23;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * (s13+s23)+
                *(pspin + m3 * 3 + l) * J * (s12+s23);
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * (s13+s23) +
                *(pspin + m3 * 3 + l) * J * (s12+s13);
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m1 * 3 + l) * J * (s12+s23)+
                *(pspin + m2 * 3 + l) * J * (s12+s13);
        }


        //  _
        // \/
        //

        if (i % N == 0 && 0 <= i / N && i / N <= N - 2){
            m1 = i;
            m2 = i + N;
            m3 = i + N + (N - 1);
        }
        else if (i == N * (N - 1)){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N + (N - 1);
        }
        else if (N * (N - 1) + 1 <= i && i <= N * (N - 1) + N - 1){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N - 1;
        }
        else{
            m1 = i;
            m2 = i + N;
            m3 = i + N - 1;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s23 = dot3(pspin + m2 * 3, pspin + 3 * m3);
        energy += s12*s13 + s12*s23 + s13*s23;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * (s13+s23)+
                *(pspin + m3 * 3 + l) * J * (s12+s23);
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * (s13+s23) +
                *(pspin + m3 * 3 + l) * J * (s12+s13);
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m1 * 3 + l) * J * (s12+s23)+
                *(pspin + m2 * 3 + l) * J * (s12+s13);
        }
    }
    energy *= -J;
    return Py_BuildValue("d", energy);
}


static PyObject* i24grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    double J;

    if (!PyArg_ParseTuple(args, "DOO", &J, &spins, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

//    int k;
    int l;

    int i=0;
    int m1=0;
    int m2=0;
    double energy = 0.0;

    int n_atoms = PyArray_DIMS(spins)[0];
    int N = (int) sqrt((double) n_atoms);

    double s12=0.0;

    for(i = 0; i < n_atoms; i++){
        //
        // /\
        // -
        if((i+1) % N == 0 &&  N - 1 <= i && i <= (N-1)*N + N - 1){
            m1 = i;
            m2 = i - (N-1);
        }
        else{
            m1 = i;
            m2 = i + 1;
        }

        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12*s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * 2.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * 2.0;
        }
        //
        // /
        //
        if (N * (N - 1) <= i && i <= N * (N - 1) + N - 1){
            m1 = i;
            m2 = i - (N - 1) * N;
        }
        else{
            m1 = i;
            m2 = i + N;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12*s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * 2.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * 2.0;
        }
        //
        // |
        //

        if (i % N == 0 && 0 <= i / N  && i / N <= N - 2){
            m1 = i;
            m2 = i + N + (N - 1);
        }
        else if  (i == N * (N - 1)){
            m1 = i;
            m2 = i - (N - 1) * N + N - 1;
        }
        else if  (N * (N - 1) + 1 <= i && i <= N * (N - 1) + N - 1){
            m1 = i;
            m2 = i - (N - 1) * N - 1;
        }
        else{
            m1 = i;
            m2 = i + N - 1;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12*s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * 2.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * 2.0;
        }


    }
    energy *= -J;
    return Py_BuildValue("d", energy);
}


static PyObject* t1_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *nndist;

    double T1;

    if (!PyArg_ParseTuple(
            args, "DOOOOOOOO", &T1, &spins, &nbl1, &nbl2, &nndist,
            &positions, &offsets, &cell, &grad))

        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnndist = DOUBLEP(nndist);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist=0.0;

    double g_temp[3] = {0.0, 0.0, 0.0};

    int k;
    int i1;
    int i2;
    int l;

    int i1_temp = n1[0];
    int neighb_numb = 0;
    double dot_prod = 0.0;
    double energy = 0.0;
    int n_spins = PyArray_DIMS(spins)[0];

    double *partial_energy;
    partial_energy = (double*)malloc(3 * n_spins * sizeof(double));

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        if (fabs(pnndist[0] - r_dist) > 0.01){
            continue;
        }
        if (i1_temp != i1){
            dot_prod = sqrt((double) neighb_numb + dot_prod);
            partial_energy[i1_temp] = dot_prod;
            energy += dot_prod;
            for(l = 0; l < 3; l++){
                *(pgrad + 3 * i1_temp + l) -=
                    g_temp[l] * T1 / (2.0 * dot_prod);
            }
            neighb_numb = 0;
            dot_prod = 0.0;
            g_temp[0] = 0.0;
            g_temp[1] = 0.0;
            g_temp[2] = 0.0;
            i1_temp = i1;
        }
        neighb_numb += 1;
        g_temp[0] += *(pspin + i2 * 3 + 0);
        g_temp[1] += *(pspin + i2 * 3 + 1);
        g_temp[2] += *(pspin + i2 * 3 + 2);
        dot_prod += dot3(pspin + i1 * 3, pspin + i2 * 3);
   }

    // don't forget the last spin:
    dot_prod = sqrt((double) neighb_numb + dot_prod);
    partial_energy[i1_temp] = dot_prod;
    energy += dot_prod;
    for(l = 0; l < 3; l++){
        *(pgrad + 3 * i1_temp + l) -= g_temp[l] * T1 / (2.0 * dot_prod);
    }

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                         pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        if (fabs(pnndist[0] - r_dist) > 0.01){
            continue;
        }
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) -= *(pspin + 3 * i2 + l)* T1 / (2.0 * partial_energy[i2]);
        }
   }

    free(partial_energy);
    energy *= -T1;
    return Py_BuildValue("d", energy);
}


static PyObject* k44grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    double J;

    if (!PyArg_ParseTuple(args, "DOO", &J, &spins, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

//    int k;
    int l;

    int i=0;
    int m1=0;
    int m2=0;
    int m3=0;
    int m4=0;
    double energy = 0.0;

    int n_atoms = PyArray_DIMS(spins)[0];
    int N = (int) sqrt((double) n_atoms);

//    double s12=0.0;
//    double s14=0.0;
    double s13=0.0;

//    double s34=0.0;
//    double s23=0.0;
    double s24=0.0;

    for(i = 0; i < n_atoms; i++){
        //  _
        // /_/
        if ((i+1)%N == 0 && N-1 <= i && i <= (N-1)*N-1){
            m1 = i;
            m2 = i - (N-1);
            m3 = i + N - (N-1);
            m4 = i + N;
        }
        else if (N*(N - 1) <= i && i < N*(N - 1) + N -1){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N-1)*N + 1;
            m4 = i - (N-1)*N;
        }
        else if (i == N*(N-1) + (N-1)){
            m1 = i;
            m2 = i - (N-1);
            m3 = i - (N-1) - (N*N-N);
            m4 = i - (N*N-N);
        }
        else{
            m1 = i;
            m2 = i + 1;
            m3 = i + 1 + N;
            m4 = i + N;
        }

        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m2 * 3 + l) * J * s13;
        }
        //  _
        // \_\

        if (i % N == 0 && 0 <= i / N  && i / N <= N - 2){
            m1 = i;
            m2 = i + 1;
            m3 = i + N;
            m4 = i + N + (N - 1);
        }
        else if (N * (N - 1) + 1 <= i && i < N * (N - 1) + N - 1){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N-1)*N;
            m4 = i - (N-1)*N - 1;
        }
        else if ((i+1)%N==0 && N-1<=i && i<=(N-1)*N-1){
            m1 = i;
            m2 = i - (N - 1);
            m3 = i + N;
            m4 = i + N - 1;
        }
        else if (i==(N+1)*(N-1)){
            m1 = i;
            m2 = i - (N - 1);
            m3 = i - (N + 1)* (N - 1) + N - 1;
            m4 = i - (N + 1)* (N - 1) + N - 1 - 1;
        }
        else if (i==N*(N-1)){
            m1 = i;
            m2 = i + 1;
            m3 = i - (N - 1) * N;
            m4 = i - (N - 1) * N + (N - 1);
        }
        else{
            m1 = i;
            m2 = i + 1;
            m3 = i + N;
            m4 = i + N - 1;
        }
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m2 * 3 + l) * J * s13;
        }

        // /\
        // \/
        if (i % N == 0 && 0 <= i / N  && i / N <= N - 3){
            m1 = i;
            m2 = i + N;
            m3 = i + 2 * N - 1 + N;
            m4 = i + 2 * N - 1;
        }
        else if (i%N==0 && i/N==N-2){
            m1 = i;
            m2 = i + N;
            m3 = i - N * (N - 2) + N - 1;
            m4 = i + N + N - 1;
        }
        else if (i == N * (N - 1)){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N + (N - 1) + N;
            m4 = i - (N - 1) * N + (N - 1);
        }
        else if (N*(N-1)+1<=i && i<=N*(N-1)+N-1){
            m1 = i;
            m2 = i - (N - 1) * N;
            m3 = i - (N - 1) * N + N - 1;
            m4 = i - (N - 1) * N - 1;
        }
        else if (N*(N-2)+1<=i && i<N*(N-1)+N-1){
            m1 = i;
            m2 = i + N;
            m3 = i + N - 1 - (N - 1) * N;
            m4 = i + N - 1;
        }
        else{
            m1 = i;
            m2 = i + N;
            m3 = i + 2 * N - 1;
            m4 = i + N - 1;
        }
        s13 = dot3(pspin + m1 * 3, pspin + 3 * m3);
        s24 = dot3(pspin + m2 * 3, pspin + 3 * m4);
        energy += s13*s24;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m3 * 3 + l) * J * s24;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m4 * 3 + l) * J * s13;
            *(pgrad + 3 * m3 + l) -=
                *(pspin + m1 * 3 + l) * J * s24;
            *(pgrad + 3 * m4 + l) -=
                *(pspin + m2 * 3 + l) * J * s13;
        }


    }
    energy *= -J;
    return Py_BuildValue("d", energy);
}


static PyObject* b3grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    double J;

    if (!PyArg_ParseTuple(args, "DOO", &J, &spins, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

//    int k;
    int l;

    int i=0;
    int m1=0;
    int m2=0;
    double energy = 0.0;

    int n_atoms = PyArray_DIMS(spins)[0];
    int N = (int) sqrt((double) n_atoms);

    double s12=0.0;

    for(i = 0; i < n_atoms; i++){
        //
        // /\
        // -
        if((i+1) % N == 0 &&  N - 1 <= i && i <= (N-1)*N + N - 1){
            m1 = i;
            m2 = i - (N-1);
        }
        else{
            m1 = i;
            m2 = i + 1;
        }

        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12 * s12 * s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * s12 * 3.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * s12 * 3.0;
        }
        //
        // /
        //
        if (N * (N - 1) <= i && i <= N * (N - 1) + N - 1){
            m1 = i;
            m2 = i - (N - 1) * N;
        }
        else{
            m1 = i;
            m2 = i + N;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12 * s12 * s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * s12 * 3.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * s12 * 3.0;
        }
        //
        // |
        //

        if (i % N == 0 && 0 <= i / N  && i / N <= N - 2){
            m1 = i;
            m2 = i + N + (N - 1);
        }
        else if  (i == N * (N - 1)){
            m1 = i;
            m2 = i - (N - 1) * N + N - 1;
        }
        else if  (N * (N - 1) + 1 <= i && i <= N * (N - 1) + N - 1){
            m1 = i;
            m2 = i - (N - 1) * N - 1;
        }
        else{
            m1 = i;
            m2 = i + N - 1;
        }
        s12 = dot3(pspin + m1 * 3, pspin + 3 * m2);
        energy += s12 * s12 * s12;
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * m1 + l) -=
                *(pspin + m2 * 3 + l) * J * s12 * s12 * 3.0;
            *(pgrad + 3 * m2 + l) -=
                *(pspin + m1 * 3 + l) * J * s12 * s12 * 3.0;
        }


    }
    energy *= -J;
    return Py_BuildValue("d", energy);
}


static PyObject* t1epsilon_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *nndist;

    PyArrayObject *arrayT1;
    PyArrayObject *arrayepsilon;

    if (!PyArg_ParseTuple(
            args, "OOOOOOOOOO", &arrayT1, &arrayepsilon,
            &spins, &nbl1, &nbl2, &nndist,
            &positions, &offsets, &cell, &grad))

        return NULL;

    double* pT1 = DOUBLEP(arrayT1);
    double* pEPS = DOUBLEP(arrayepsilon);
    int nshells = PyArray_DIMS(arrayT1)[0];

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnndist = DOUBLEP(nndist);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist=0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me;

    double energy = 0.0;
    double epsilon = pEPS[0];
    double T1 = pT1[0];
    double eps2 = epsilon - 1.0;
    int n_spins = PyArray_DIMS(spins)[0];

//    double *partial_energy;
//    partial_energy = (double*) calloc(n_spins, sizeof(double));

    int i;
    double** partial_energy;
    partial_energy = calloc(nshells, sizeof(double*));
    for(i = 0; i < nshells; i++) {
        partial_energy[i] = calloc(n_spins, sizeof(double));
    }

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));

        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }

        partial_energy[me][i1] +=
            dot3(pspin + i1 * 3, pspin + i2 * 3) + 1.0;
   }

    for(k = 0; k < n_spins; k++){
        for (l = 0; l < nshells; l++){
            epsilon = pEPS[l];
            eps2 = epsilon - 1.0;
            T1 = pT1[l];
            energy -= T1 * pow(partial_energy[l][k], epsilon);
        }
    }

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                         pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                epsilon = pEPS[me];
                eps2 = epsilon - 1.0;
                T1 = pT1[me];
                break;
            }
        }

        for(l = 0; l < 3; l++){
            // from itself and other neighbours
            Py_INCREF(grad);
            *(pgrad + 3 * i1 + l) -=
                *(pspin + 3 * i2 + l) * T1 *
                epsilon * (
                pow(partial_energy[me][i1], eps2) +
                pow(partial_energy[me][i2], eps2));
            Py_DECREF(grad);
        }
   }

    for (i=0; i< nshells; i++) {
    	free(partial_energy[i]);
    }
    free(partial_energy);

    return Py_BuildValue("d", energy);
}


static PyObject* exponentail_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *nndist;
    PyArrayObject *nnJ;
    PyArrayObject *arrayepsilon;

    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *grad;

    if (!PyArg_ParseTuple(
            args, "OOOOOOOOOO",
            &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell,
            &nndist, &nnJ, &arrayepsilon, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pnndist = DOUBLEP(nndist);
    double* pnnJ = DOUBLEP(nnJ);
    double* pgrad = DOUBLEP(grad);
    double* pEPS = DOUBLEP(arrayepsilon);


    int nshells = PyArray_DIMS(nnJ)[0];

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist = 0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me=0;

    double pair_en = 0.0;
    double pair_int = 0.0;
    double energy = 0.0;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){

        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);

        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }
        pair_int = dot3(pspin + i1 * 3, pspin + i2 * 3);
        pair_en = -pnnJ[me] * exp((pair_int - 1.0)/pEPS[me]);
        energy += pair_en;

        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) +=
                *(pspin + i2 * 3 + l) * pair_en/pEPS[me];
        }
    }

    energy *= 0.5;
    return Py_BuildValue("d", energy);
}


static PyObject* exponentail_sum_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *nndist;

    PyArrayObject *arrayT1;
    PyArrayObject *arrayepsilon;

    if (!PyArg_ParseTuple(
            args, "OOOOOOOOOO", &arrayT1, &arrayepsilon,
            &spins, &nbl1, &nbl2, &nndist,
            &positions, &offsets, &cell, &grad))

        return NULL;

    double* pT1 = DOUBLEP(arrayT1);
    double* pEPS = DOUBLEP(arrayepsilon);
    int nshells = PyArray_DIMS(arrayT1)[0];

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnndist = DOUBLEP(nndist);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist=0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me;

    double energy = 0.0;
    double epsilon = pEPS[0];
    double T1 = pT1[0];
    int n_spins = PyArray_DIMS(spins)[0];

//    double *partial_energy;
//    partial_energy = (double*) calloc(n_spins, sizeof(double));

    double** partial_energy;
    partial_energy = calloc(nshells, sizeof(double*));
    int i = 0;
    for(i = 0; i < nshells; i++) {
        partial_energy[i] = calloc(n_spins, sizeof(double));
    }

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));

        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }

        partial_energy[me][i1] +=
            dot3(pspin + i1 * 3, pspin + i2 * 3) - 1.0;
   }
   double tmp_v1 = 0.0;
   double tmp_v2 = 0.0;

    for(k = 0; k < n_spins; k++){
        for (l = 0; l < nshells; l++){
            epsilon = pEPS[l];
            T1 = pT1[l];
            tmp_v1 = partial_energy[l][k]/epsilon;
            energy -= T1 * exp(tmp_v1);
        }
    }

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                         pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                epsilon = pEPS[me];
                T1 = pT1[me];
                break;
            }
        }

        for(l = 0; l < 3; l++){
            // from itself and other neighbours
            tmp_v1 = partial_energy[me][i1]/epsilon;
            tmp_v2 = partial_energy[me][i2]/epsilon;
            Py_INCREF(grad);
            *(pgrad + 3 * i1 + l) -=
                *(pspin + 3 * i2 + l) *
                (T1 / epsilon) * (exp(tmp_v1) + exp(tmp_v2));
            Py_DECREF(grad);
        }
   }

    for (i=0; i< nshells; i++) {
    	free(partial_energy[i]);
    }
    free(partial_energy);

    return Py_BuildValue("d", energy);
}


static PyObject* biquadratic_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *nndist;
    PyArrayObject *nnJ;

    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *grad;
    PyArrayObject *biquadr_interaction_sums;


    if (!PyArg_ParseTuple(
            args, "OOOOOOOOOO",
            &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell,
            &nndist, &nnJ, &grad,
            &biquadr_interaction_sums))
        return NULL;

    double* p_biquadr_interaction_sums = DOUBLEP(biquadr_interaction_sums);
    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pnndist = DOUBLEP(nndist);
    double* pnnJ = DOUBLEP(nnJ);
    double* pgrad = DOUBLEP(grad);

    int nshells = PyArray_DIMS(nnJ)[0];

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist = 0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me=0;

    double pair_int = 0.0;
    double energy = 0.0;
    double tmp_v1 = 0.0;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){

        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);

        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }
        pair_int = dot3(pspin + i1 * 3, pspin + i2 * 3);
        tmp_v1 = pow(pair_int, 2.0);
        energy += -pnnJ[me] * tmp_v1;
        p_biquadr_interaction_sums[me] += tmp_v1;

        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) +=
                *(pspin + i2 * 3 + l) * pair_int * 2.0 * (-pnnJ[me]);
        }
    }
    for (l = 0; l < nshells; l++){
        p_biquadr_interaction_sums[l] *= 0.5;
    }

    energy *= 0.5;
    return Py_BuildValue("d", energy);
}


static PyObject* cross_dot_grad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *nndist;
    PyArrayObject *nnJ;
    PyArrayObject *arrayepsilon;
    PyArrayObject *arrayT;

    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    PyArrayObject *grad;

    if (!PyArg_ParseTuple(
            args, "OOOOOOOOOOO",
            &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell,
            &nndist, &nnJ, &arrayepsilon, &arrayT, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pnndist = DOUBLEP(nndist);
    double* pnnJ = DOUBLEP(nnJ);
    double* pgrad = DOUBLEP(grad);
    double* pEPS = DOUBLEP(arrayepsilon);
    double* pT = DOUBLEP(arrayT);


    int nshells = PyArray_DIMS(nnJ)[0];

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);

    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist = 0.0;

    int k;
    int i1;
    int i2;
    int l;
    int me=0;

    double pair_int = 0.0;
    double energy = 0.0;
    double cross_prod[3] = {0.0, 0.0, 0.0};
    double cross_square = 0.0;
    double eps2 = 0.0;

    double tmp_v1 = 0.0;
    double tmp_v2 = 0.0;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){

        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);

        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        r_dist = sqrt(dot3(r_ij, r_ij));
        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me  = l;
                break;
            }
        }
        pair_int = dot3(pspin + i1 * 3, pspin + i2 * 3);
        cross(pspin + i1 * 3, pspin + i2 * 3, cross_prod);
        cross_square = dot3(cross_prod, cross_prod);

//        pair_en = -pnnJ[me] * exp((pair_int - 1.0)/pEPS[me]);
        energy -= pnnJ[me] * (1.0 - pT[me] * pow(cross_square, pEPS[me])) * pair_int;

        eps2 = pEPS[me] - 1.0;
        for(l = 0; l < 3; l++){
            tmp_v1 = pnnJ[me] * ( pT[me] * (-2.0) * pEPS[me] * pow(cross_square, eps2)) * pair_int;
            tmp_v2 = pnnJ[me] * (1.0 - pT[me] * pow(cross_square, pEPS[me]));
            *(pgrad + 3 * i1 + l) -=
                *(pspin + i2 * 3 + l) * tmp_v2 +
                tmp_v1 * ( *(pspin + i1 * 3 + l) - *(pspin + i2 * 3 + l) * pair_int);


        }
    }

    energy *= 0.5;
    return Py_BuildValue("d", energy);
}


static PyMethodDef GradMethods[] =
{
     {"xgrad", xgrad, METH_VARARGS, "evaluate xgrad"},
     {"dmgrad", dmgrad, METH_VARARGS, "evaluate dmgrad"},
     {"angrad", angrad, METH_VARARGS, "evaluate angrad"},
     {"zgrad", zgrad, METH_VARARGS, "evaluate zgrad"},
     {"xnn_grad", xnn_grad, METH_VARARGS, "evaluate xnn_grad"},
     {"i24grad", i24grad, METH_VARARGS, "evaluate i24grad"},
     {"i34grad", i34grad, METH_VARARGS, "evaluate i34grad"},
     {"i44grad", i44grad, METH_VARARGS, "evaluate i44grad"},
     {"t1grad", t1_grad, METH_VARARGS, "evaluate t1grad"},
     {"k44grad", k44grad, METH_VARARGS, "evaluate k44grad"},
     {"b3grad", b3grad, METH_VARARGS, "evaluate b3grad"},
     {"t1epsilon_grad", t1epsilon_grad, METH_VARARGS, "evaluate t1epsilon_grad"},
     {"exponentail_grad", exponentail_grad, METH_VARARGS, "evaluate exponentail_grad"},
     {"exponentail_sum_grad", exponentail_sum_grad, METH_VARARGS, "evaluate exponentail_sum_grad"},
     {"biquadratic_grad", biquadratic_grad, METH_VARARGS, "evaluate biquadratic_grad"},
     {"cross_dot_grad", cross_dot_grad, METH_VARARGS, "evaluate cross_dot_grad"},
     {NULL, NULL, 0, NULL}
};


static struct PyModuleDef cModPyDem =
{
    PyModuleDef_HEAD_INIT,
    "gradient_module", "Some documentation",
    -1,
    GradMethods
};

PyMODINIT_FUNC
PyInit_gradient_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}
