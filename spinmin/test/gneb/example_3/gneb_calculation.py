from spinmin.gneb.gneb import GNEB
from spinmin.utilities import from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
import numpy as np

# a = 2.87
a = 1.0 # /0.866025
J = 25.6

interactions = {'J': 25.6/J,
                'K_ad': {'ampl': 1.2/J,
                         'dir': [0.0, 1.0, 0.0]},
                'K2_ad': {'ampl': -0.5/J,
                          'dir': [0.0, 0.0, 1.0]}
                }

# reads in the first minimum
atoms_i = io.read('initial_state.traj')
spins_i = atoms_i.get_initial_magnetic_moments()
ham_i = SpinHamiltonian(atoms_i, spins_i, interactions)

# reads in the second minimum
atoms_f = io.read('final_state.traj')
spins_f = atoms_f.get_initial_magnetic_moments()
ham_f = SpinHamiltonian(atoms_f, spins_f, interactions)

# initialise the GNEB class
gneb = GNEB(spring=[2.0, 1.0])

# creates a chain of images with number of images (noi) and linearly interpolates the spins
gneb.interpolate_chain(ham_i, ham_f, noi=16)

# initialises the Unitary Optimisation and runs the calculation for the minimum energy path
opt = UnitaryOptimisation(gneb, max_iter=30000, convergence=10**-4)

opt.run()


# this is example of how to run gneb with SIB

# from spinmin.dynamics.dynamics import SpinDynamics
# opt = SpinDynamics(gneb, dt=200, damping_only=True, n_steps=10000)
# opt.run()

# plots the energy relative to the initial state as a function of the reaction coordinates

gneb.plot_interpolation(name='Flat Barrier')

# plots the Images
# gneb.plot_chain('FinalImage')

# saves the chain as a list of Atom objects
gneb.write('final_path_flat.traj')

energy = [0.,         2.31315102, 3.79899351,
          4.05366146, 4.09014819, 4.09571096,
          4.09657532, 4.09670719, 4.09670708,
          4.09658263, 4.09575725, 4.09039344,
          4.05488199, 3.80465646, 2.32532499,
          0.        ]
print(np.allclose(energy, gneb.e_total - gneb.e_total[0]))