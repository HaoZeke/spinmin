from ase.build import hcp0001
import numpy as np
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.ncaa.hamiltonian import NCAAHamiltonian
from spinmin.utilities import collinear_state, skyrm_function, plot_xy
from ase import io

a = 1.0
atoms = hcp0001('Fe', a=a, orthogonal=False, size=(11, 11, 1))
spins = collinear_state(len(atoms))

ham = NCAAHamiltonian(atoms, spins,
                  interactions={'E0': -12.0,
                                'U': 13.0,
                                'V': 0.5
                                },
                  convergence=1.0e-4,
                  maxiter=1000,
                  txt='output.txt',
                  method='SD')
opt = UnitaryOptimisation(ham, linesearch_algo='CutOff',
                          max_iter=100, convergence=1.0e-8)
opt.run()

plot_xy(atoms, spins, 'final_state')
atoms.set_initial_magnetic_moments(spins)
io.write('final_state.traj', atoms)
