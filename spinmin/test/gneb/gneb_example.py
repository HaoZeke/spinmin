from spinmin.gneb.gneb import GNEB
from spinmin.utilities import from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from ase import io

a = 2.51
interactions={'J': 29.0,
              'DM': {'ampl': 1.5,
                     'dir': [0.0, 0.0, 1.0]},
              # 'Z_ad': {'ampl': from_mT_to_meV(2.1, 250),
              #          'dir': [0.0, 0.0, 1.0]},
              'K_ad': {'ampl': 0.293,
                       'dir': [0.0, 0.0, 1.0]},
              'r_c': a/2+0.1}
# first minimum
atoms_i = io.read('init_state.traj')
spins_i = atoms_i.get_initial_magnetic_moments()
ham_i = SpinHamiltonian(atoms_i, spins_i, interactions)

# second minimum
atoms_f = io.read('final_state.traj')
spins_f = atoms_f.get_initial_magnetic_moments()
ham_f = SpinHamiltonian(atoms_f, spins_f, interactions)

gneb = GNEB(spring=0.1)
gneb.interpolate_chain(ham_i, ham_f, noi=8)
gneb.plot_chain()

from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
opt = UnitaryOptimisation(gneb, convergence=1.0e-7)

opt.run()

# this is example of how to run gneb with SIB
# from spinmin.dynamics.dynamics import SpinDynamics
# opt = SpinDynamics(gneb, dt=200, damping_only=True, n_steps=10000)
# opt.run()

gneb.plot_chain(name='FinalFig')
gneb.plot_interpolation()
