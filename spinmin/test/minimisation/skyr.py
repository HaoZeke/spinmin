from ase.build import bcc100
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.utilities import plot_xy, skyrm_function
import numpy as np

atoms = bcc100('Fe', a=1.0, orthogonal=True, size=(20, 20, 1))
# spins = random_spins(len(atoms), seed=7)
spins = skyrm_function(atoms, 10)
atoms.set_initial_magnetic_moments(spins)

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': 10.0,
                                    'DM': 5.0,
                                    'Z_ad': {'ampl': 2.0,
                                             'dir': [0.0, 0.0, 1.0]}
                                    })

opt = UnitaryOptimisation(ham)
opt.run()
energy = ham.get_energy_and_gradients()[0]
print(np.allclose(energy,-8792.747330389097))
plot_xy(atoms, spins)