import numpy as np
from spinmin.units import BohrMagn
from scipy.linalg import expm

def skh2u(upp_tr, real=True):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.eye(3)

    evals, evec = egdecomp_skhm(upp_tr)

    u_mat = (evec * np.exp(evals)) @ evec.T.conj()

    if real is True:
        u_mat = u_mat.real

    return u_mat


def egdecomp_skhm(upp_tr):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.array([0.0, 0.0, 0.0]), np.eye(3)

    x = np.sqrt(np.dot(upp_tr, upp_tr))
    a = upp_tr[0]
    b = upp_tr[1]
    c = upp_tr[2]

    v1 = np.array([c/x, -b/x, a/x])
    ac = np.sqrt(a**2 + c**2)

    if ac < 1.0e-12:
        if b < 0.0:
            v2 = np.array([-1.0j, 0.0, 1.0]) / np.sqrt(2.0)
        else:
            v2 = np.array([1.0j, 0.0, 1.0]) / np.sqrt(2.0)
    else:
        v2 = np.array([b * c + 1.0j * a * x,
                       a**2 + c**2,
                       a * b - 1.0j * c * x]) / \
             (x * np.sqrt(2.0) * ac)

    v3 = v2.conj()

    return np.array([0.0, -1.0j*x, 1.0j*x]), \
           np.array([v1, v2, v3]).T


def get_grad(upp_tr, m_uptr):

    m_cc = np.zeros(shape=(3, 3))
    m_cc[([1, 2, 0], [2, 0, 1])] = m_uptr
    m_cc += -m_cc.T

    evals, evec = egdecomp_skhm(upp_tr)

    grad = np.dot(evec.T.conj(), np.dot(m_cc, evec))
    grad = grad * D_matrix(1.0j * evals)
    grad = np.dot(evec, np.dot(grad, evec.T.conj()))
    return grad[([0, 0, 1], [1, 2, 2])].real


def D_matrix(omega):

    m = omega.shape[0]
    u_m = np.ones(shape=(m, m))

    u_m = omega[:, np.newaxis] * u_m - omega * u_m

    with np.errstate(divide='ignore', invalid='ignore'):
        u_m = 1.0j * np.divide(np.exp(-1.0j * u_m) - 1.0, u_m)

    u_m[np.isnan(u_m)] = 1.0
    u_m[np.isinf(u_m)] = 1.0

    return u_m


def random_spins(n_atoms, length=1.0, seed=None):

    if seed is not None:
        assert type(seed) == int
        np.random.seed(seed)
    spins = np.zeros(shape=(n_atoms, 3))
    for i, s in enumerate(spins):
        z = np.random.uniform(-1.0, 1.0)
        phi = np.random.uniform(0.0, 2.0 * np.pi)
        x = np.sqrt(1.0 - z ** 2) * np.cos(phi)
        y = np.sqrt(1.0 - z ** 2) * np.sin(phi)
        spins[i] = length * np.array([x, y, z])
    return spins


def plot_xy(atoms, spins, name='Figure', component=2, scale=1, cmap="jet_r"):

    import matplotlib.pyplot as plt
    pos = atoms.positions
    # plt.figure(figsize=(8, 6))
    plt.axis('off')
    plt.axis('equal')
    # plt.xlabel(r'x coordinate ($\AA$)')
    # plt.ylabel(r'y coordinate ($\AA$)')
    plt.quiver(pos[:, 0], pos[:, 1], spins[:, 0], spins[:, 1], spins[:, component],
               cmap=cmap,
               # scale_units="inches",
               scale=scale,
               width=0.005,
               pivot="mid",
               angles = 'xy',
               scale_units = 'xy',
               )
    cbar = plt.colorbar()
    # cbar.ax.set_ylabel(r'$S_z$', rotation=0)
    plt.clim(-1, 1)
    plt.savefig(name + '.png', bbox_inches='tight', pad_inches=0, dpi=500)
    plt.close()


def read_from_txt(name, n_atoms):

    spins = np.zeros(shape=(n_atoms, 3))
    f = open(name + '.txt', 'r')
    i = 0
    for l in f:
        x = l.rstrip('\n')
        x = x.split()
        spins[i] = np.array([float(x[0]), float(x[1]), float(x[2])])
        i += 1
    f.close()

    return spins


def collinear_state(n_atoms, type='f', along='z'):

    spins = np.zeros(shape=(n_atoms, 3))
    if along == 'x':
        d = 0
    elif along == 'y':
        d = 1
    elif along == 'z':
        d = 2
    else:
        raise ValueError('along can be x, y or z')

    if type == 'f':
        spins[:, d] = 1.0
    elif type == 'af':
        spins[::2, d] = 1.0
        spins[1::2, d] = -1.0

    return spins


def from_mT_to_meV(magmom, H):

    return magmom * BohrMagn * H


def from_Kelv_to_meV(T):
    return T * 8.621738e-2


def theta_function(atoms, r, r0=None, length=1.0):

    """
    ferromagnetic background along z direction
    :param atoms:
    :param r: radius theta function
    :param r0: the center of theta function
    :param length: length of magnetic moments
    :return:
    """

    #TODO: does it work on a boundary?
    assert length > 0.0
    pos = atoms.positions
    if r0 is None:
        r0 = atoms.get_center_of_mass()

    d = [np.linalg.norm(r0-item) for item in pos]
    spins=np.array([[0.0, 0.0, length]] * len(atoms))
    spins[[i for i in range(len(spins))
           if d[i] < r]]=np.array([0.0,0.0,-length])

    return spins


def save_csv(atoms, spins, name='magnetization.csv'):
    import csv
    pos = atoms.positions
    with open(name, 'w', newline='') as csvfile:

        spamwriter = csv.writer(
            csvfile, delimiter=' ', quotechar='|',
            quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['N','x','y','z','s_x','s_y','s_z'])
        for i in range(len(pos)):
            spamwriter.writerow([i]+pos[i].tolist()+spins[i].tolist())


def sk_radius(atoms, spins):

    """
    assume that ferromagnetic background points to z-direction

    :param atoms:
    :param spins:
    :return: Effective radius of skyrmion using integral
    """
    # FIXME: skyrmion must be in around of the cell centre
    #  works only for one skyrmion

    pos = atoms.positions
    r0 = atoms.get_center_of_mass()
    dr0 = np.linalg.norm(r0)*0.5
    c_c = np.average(
        pos, axis=0,
        weights=[np.maximum(0.0,-spins[item,2]) *
                 float(np.linalg.norm(r0-pos[item])<dr0)
                 for item in range(len(atoms))])
    d = [np.linalg.norm(c_c-item) for item in pos]
    sortedz = [x for _,x in sorted(zip(d, enumerate(spins[:,2])))]
    s = 0.0
    i = 0
    while s <= 0.0:
        s += sortedz[i][1]
        i += 1
    r_eff = d[sortedz[i][0]]

    print('Effective radius of the skyrmion = ', r_eff)

    return r_eff


def AtoJ(a, A):
    """

    :param a: effective size of primitive cell in Ang
    :param A: micromagnetic exchange Joule/m, all pairs taken twie
    :return: J in meV all pairse taken once
    """

    return 2.0 * a * A * 6.24150974e11


def K_micro_toK_atom(a, K):
    """

    :param a: effective size of primitive cell in Ang
    :param K: Joule/m^3
    :return: meV
    """

    return a**3.0 * K * 6.24150974e18 * 1000.0 / (1.0e30)


def Kmicro(coef, m0Ms):
    mu0 = 4 * np.pi * 1.0e-7

    return coef * (m0Ms)**2/mu0


def lexmicro(A, mu0Ms):
    """
    :param A: Joule/M
    :param mu0Ms: T
    :return: l_ex in Angstrom
    """
    mu0 = 4 * np.pi * 1.0e-7
    return np.sqrt(2. * mu0 * A / (mu0Ms)**2.) * 1.0e10


def D_micro_toD_atom(a, D):
    """

    :param a: effective size of primitive cell in Ang
    :param D: J/m^2
    :return:
    """

    return a**2.0 * D * 6.24150974e18 * 1000.0 / (1.0e20)


def Mstomu(a, Ms):
    """
    magnetic moment in units of Bohr magneton of primitive cell
    :param a: effective size of primitive cell in Ang
    :param Ms: A/m
    :return:
    """

    return 6.24150974e-12 * Ms * a**3.0 / BohrMagn


def mu0Mstomu(a, mu0Ms):
    """
    magnetic moment in units of Bohr magneton of primitive cell
    :param a: effective size of primitive cell in Ang
    :param Ms: T
    :return:
    """
    mu0 = 4 * np.pi * 1.0e-7

    return 6.24150974e-12 * mu0Ms * a**3.0 / (BohrMagn*mu0)


def save_xyz(atoms, spins=None, file_name_addition=''):
    add = file_name_addition
    if spins is None:
        spins = atoms.get_initial_magnetic_moments()
    file = open('atoms_' + add + '.txt', 'w')
    pos = atoms.positions
    for x in pos:
        print(x[0], x[1], x[2], file=file)
    file.close()

    file = open('spins_' + add + '.txt', 'w')
    for x in spins:
        print(x[0], x[1], x[2], file=file)
    file.close()


def skyrm_function(atoms, r, spins=None, r0=None, length=1.0, phase=0.0):

    """
    ferromagnetic background along z direction
    :param atoms:
    :param r: radius theta function
    :param r0: the center of theta function
    :param length: length of magnetic moments
    :return:
    """

    #TODO: does it work on a boundary?
    assert length > 0.0
    pos = atoms.positions
    if r0 is None:
        r0 = atoms.get_center_of_mass()

    d = np.asarray([np.linalg.norm(r0-item) for item in pos])
    x = d < r

    coord_center = (pos - r0)[x]
    rho_center = d[x]
    phi = np.arctan2(coord_center[:,1], coord_center[:,0])  # change arguments to get antiskyrmion
    theta = np.pi*(1.0 - rho_center/r)
    phi2 = phi + phase

    if spins is None:
        spins = collinear_state(len(atoms))

    spins[x] = np.asarray([
        np.sin(theta) * np.cos(phi2) * spins[x][:,2],
        np.sin(theta) * np.sin(phi2) * spins[x][:,2],
        np.cos(theta) * spins[x][:,2]
    ]).T*length

    return spins


def plot_xy2(atoms, spins, size, name='Figure', levels=None):
    import matplotlib.pyplot as plt

    pos = atoms
    # plt.figure(figsize=(6, 6))
    plt.axis('off')
    # plt.xlabel(r'x coordinate ($\AA$)')
    # plt.ylabel(r'y coordinate ($\AA$)')
    if levels is None:
        plt.contourf(pos[:, 0].reshape(size[0], size[1]),
                     pos[:, 1].reshape(size[0], size[1]),
                     spins[:, 2].reshape(size[0], size[1]), cmap="bwr",  vmin=-1.0, vmax=1.)
    else:
        plt.contourf(pos[:, 0].reshape(size[0], size[1]),
                     pos[:, 1].reshape(size[0], size[1]),
                     spins[:, 2].reshape(size[0], size[1]), levels=levels ,cmap="bwr", vmin=-1.0, vmax=1.)
    # plt.quiver(pos[:, 0], pos[:, 1], spins[:, 0], spins[:, 1], # spins[:, 2],
    #            scale_units="inches", #  cmap="bwr",
    #            scale=5, width=0.006, pivot="mid")

    # plt.xlim(90-7, 110+7)
    # plt.ylim(76-7, 96+7)

    # plt.ylim(0, 30)
    # cbar = plt.colorbar()
    # cbar.ax.set_ylabel(r'$S_z$', rotation=0)
    plt.clim(-1, 1)

    plt.savefig(name + '.png', bbox_inches='tight', pad_inches=.1)
    plt.close()


def plot_xy3(atoms, spins, name='Figure', levels=None):
    import matplotlib.pyplot as plt
    from scipy.interpolate import griddata
    import matplotlib.cm as cm

    pos = atoms.positions[:]
    # plt.figure(figsize=(6, 6))
    plt.axis('off')
    plt.axis('equal')
    # plt.xlabel(r'x coordinate ($\AA$)')
    # plt.ylabel(r'y coordinate ($\AA$)')
    X, Y = np.meshgrid(pos[:, 0], pos[:, 1])
    Z = griddata((pos[:, 0], pos[:, 1]), spins[:, 2], (X, Y))


    colors = cm.RdBu(spins[:, 2])
    plt.scatter(pos[:, 0], pos[:, 1], color=colors, marker='H')

    # for i in range(len(pos[:, 0])):
    #     plt.scatter(pos[i][0], pos[i][1], color=colors[i], marker='H')
    # plt.contourf(X, Y, Z, levels=5)

    # plt.tricontour(pos[:, 0], pos[:, 1], Z, colors='black')
    # if levels is None:
    #     plt.contourf(pos[:, 0].reshape(size[0], size[1]),
    #                  pos[:, 1].reshape(size[0], size[1]),
    #                  spins[:, 2].reshape(size[0], size[1]), cmap="bwr",  vmin=-1.0, vmax=1.)
    # else:
    #     plt.contourf(pos[:, 0].reshape(size[0], size[1]),
    #                  pos[:, 1].reshape(size[0], size[1]),
    #                  spins[:, 2].reshape(size[0], size[1]), levels=levels ,cmap="bwr", vmin=-1.0, vmax=1.)
    # plt.quiver(pos[:, 0], pos[:, 1], spins[:, 0], spins[:, 1], # spins[:, 2],
    #            scale_units="inches", #  cmap="bwr",
    #            scale=5, width=0.006, pivot="mid")

    # plt.xlim(90-7, 110+7)
    # plt.ylim(76-7, 96+7)

    # plt.ylim(0, 30)
    # cbar = plt.colorbar()
    # cbar.ax.set_ylabel(r'$S_z$', rotation=0)
    # plt.clim(-1, 1)

    plt.savefig(name + '.png', bbox_inches='tight', pad_inches=.1)
    plt.close()


def creat3Qstate(atoms, q, length=1.0, a=1.0, rotate=False):

    q = np.asarray(q)
    spins = np.zeros(shape=(len(atoms), 3))
    rota = 120 * np.pi / 180
    cos = np.cos
    sin = np.sin

    R = np.asarray([[cos(rota), -sin(rota), 0.0],
                    [sin(rota), cos(rota), 0.0],
                    [0.0, 0.0, 1.0]])

    if rotate:
        A = np.asarray([[0.0, 0.0, 1.0 / np.sqrt(2)],
                        [0.0, 0.0, 1.0 / np.sqrt(2)],
                        [-1.0 / np.sqrt(2), -1.0 / np.sqrt(2),
                         0.0]])
        U1 = expm(0.9553166181245092 * A)

        A = np.asarray([[0.0, 1.0, 0.0],
                        [-1.0, 0.0, 0.0],
                        [0.0, 0.0, 0.0]])

        U2 = expm(5 * np.pi / 3 * A)

        U = U2 @ U1
    else:
        U = np.eye(3)

    for i in range(len(atoms)):
        pos = atoms.positions[i]
        dot1 = (2.0 * np.pi / a) * length * q @ pos
        dot2 = (2.0 * np.pi / a) * length * (R @ q) @ pos
        dot3 = (2.0 * np.pi / a) * length * (R @ R @ q) @ pos

        spins[i] = [np.cos(dot1),
                    np.cos(dot2),
                    np.cos(dot3)]


        spins[i] =  U @ spins[i]

    spins /= np.linalg.norm(spins, axis=1)[:, np.newaxis]

    return spins


def creat_sp_spiral(atoms, q, length=1.0, a=1.0, theta=np.pi/2, rot='xy'):
    q = np.asarray(q)
    spins = np.zeros(shape=(len(atoms), 3))

    for i in range(len(atoms)):
        pos = atoms.positions[i]
        dot = (2.0 * np.pi / a) * length * q @ pos
        if rot == 'xy':
            spins[i] = [np.sin(theta) * np.sin(dot),
                        np.sin(theta) * np.cos(dot), np.cos(theta)]
        elif rot == 'xz':
            spins[i] = [np.sin(theta) * np.sin(dot),
                        np.cos(theta),
                        np.sin(theta) * np.cos(dot)]
        elif rot == 'yz':
            spins[i] = [np.cos(theta),
                        np.sin(theta) * np.sin(dot),
                        np.sin(theta) * np.cos(dot)]
        else:
            raise KeyError

    return spins


def get_hess(ham):

    angles = get_angles(ham.spins)

    eps = 1.0e-4
    coef = [1.0, -1.0,]
    displ = [1.0, -1.0]
    second_der = 0.0

    ndim = ham.spins.shape[0]
    hess = np.zeros(shape=(2 * ndim, 2 * ndim))
    for i in range(len(angles)):

        for k in range(2):
            atmp = angles[i]
            angles[i] += displ[k] * eps

            if i < ndim:
                theta_ind = i
                phi_ind = i + ndim
                my_spin = i
            else:
                theta_ind = i - ndim
                phi_ind = i
                my_spin = i - ndim

            ham.spins[my_spin] = \
                np.asarray([np.sin(angles[theta_ind])*np.cos(angles[phi_ind]),
                            np.sin(angles[theta_ind])*np.sin(angles[phi_ind]),
                            np.cos(angles[theta_ind])])
            b = ham.get_gradients()

            eth, eph = tangent_vector(angles[:ndim],angles[ndim:2*ndim])
            g_sp = np.concatenate((np.sum(b * eth, axis=1), np.sum(b * eph, axis=1)*np.sin(angles[:ndim])))
            hess[i, :] += g_sp * coef[k]

            angles[i] = atmp

        hess[i, :] *= 1.0 / (2.0 * eps)

    return hess


def get_angles(spins):
    ndim = spins.shape[0]
    x = np.zeros(shape=(2*ndim))
    x[:ndim] = np.arccos(spins[:,2])
    x[ndim:2*ndim] = np.arctan2(spins[:,1],spins[:,0])
    negatives = x[ndim:2*ndim] < 0.0
    x[ndim:2 * ndim][negatives] = 2*np.pi + x[ndim:2 * ndim][negatives]
    return x


def tangent_vector(theta, phi):
    assert theta.shape == phi.shape

    eth = np.zeros(shape=(theta.shape[0], 3))
    ephi = np.zeros(shape=(theta.shape[0], 3))

    eth[:,0] = np.cos(theta) * np.cos(phi)
    eth[:,1] = np.cos(theta) * np.sin(phi)
    eth[:,2] = -np.sin(theta)

    ephi[:,0] = -np.sin(phi)
    ephi[:,1] = np.cos(phi)
    ephi[:,2] = 0.0

    return eth, ephi


def ase2spirit(atoms, spins, lat='sc', output='input.cfg', out_spins='spins.txt'):

    Cell = atoms.get_cell()
    bl = Cell.get_bravais_lattice()
    lc = bl.vars()['a']

    begining_str =\
"""############## Spirit Configuration ##############
### Output Folders
output_file_tag    <time>
log_output_folder  .
llg_output_folder  output
mc_output_folder   output
gneb_output_folder output
mmf_output_folder  output
ema_output_folder  output

### Save input parameters on State creation/deletion
save_input_initial  0
save_input_final    0

### Save atom positions on State creation/deletion
save_positions_initial 0
save_positions_final   0

### Save exchange and DM neighbours on State creation/deletion
save_neighbours_initial 0
save_neighbours_final   0

################## Hamiltonian ###################

### Hamiltonian Type
### (heisenberg_neighbours, heisenberg_pairs, gaussian)
hamiltonian                heisenberg_neighbours

### Boundary_conditions (a b c): 0(open), 1(periodical)
boundary_conditions        1 1 0

### External magnetic field vector[T]
external_field_magnitude   0.0
external_field_normal      0.0 0.0 1.0
### µSpin
mu_s                       1.0

### Uniaxial anisotropy constant [meV]
anisotropy_magnitude       0.0
anisotropy_normal          0.0 0.0 1.0

### Dipole-dipole interaction caclulation method
### (fft, fmm, cutoff, none)
ddi_method                 none

### DDI number of periodic images in (a b c)
ddi_n_periodic_images      4 4 4

### DDI cutoff radius (if cutoff is used)
ddi_radius                 0.0

### Pairs
n_interaction_pairs 3
i j   da db dc    Jij   Dij  Dijx Dijy Dijz
0 0    1  0  0   0.0   0.0   1.0  0.0  0.0
0 0    0  1  0   0.0   0.0   0.0  1.0  0.0
0 0    0  0  1   0.0   0.0   0.0  0.0  1.0

################ End Hamiltonian #################

################### Geometry #####################
### Lattice constant for basis and translations
lattice_constant {:f}

### The bravais lattice type
bravais_lattice {:}
# bravais_lattice hex2d 
### Number of basis cells along principal
### directions (a b c)
n_basis_cells 1 1 1

basis 
""".format(lc * 1.7 * len(atoms)**(1./3), lat)

    coord = str(len(atoms)) + '\n'

    v = 1.05 * Cell.volume**(1./3)
    if v < 1.0e-4:
        v = lc*1.05
    pos = atoms.get_positions() / v
    for r in pos:
        i = 0
        for x in r:
            if i != 2:
                coord += str(x) + ' '
            else:
                coord += str(x) + '\n'
            i+=1
    end_str = \
"""
################# End Geometry ###################

############### Logging Parameters ###############
### Levels of information
# 0 = ALL     - Anything
# 1 = SEVERE  - Severe error
# 2 = ERROR   - Error which can be handled
# 3 = WARNING - Possible unintended behaviour etc
# 4 = PARAMETER - Input parameter logging
# 5 = INFO      - Status information etc
# 6 = DEBUG     - Deeper status, eg numerical

### Print log messages to the console
log_to_console    1
### Print messages up to (including) log_console_level
log_console_level 5

### Save the log as a file
log_to_file    1
### Save messages up to (including) log_file_level
log_file_level 4
############# End Logging Parameters #############

################# MC Parameters ##################
### Maximum wall time for single simulation
### hh:mm:ss, where 0:0:0 is infinity
mc_max_walltime     0:0:0

### Seed for Random Number Generator
mc_seed             20006

### Number of iterations
mc_n_iterations     2000000
### Number of iterations after which to save
mc_n_iterations_log 2000

### Temperature [K]
mc_temperature      0

### Acceptance ratio
mc_acceptance_ratio 0.5

### Output configuration
mc_output_any     1
mc_output_initial 1
mc_output_final   1

mc_output_energy_step                  0
mc_output_energy_archive               1
mc_output_energy_spin_resolved         0
mc_output_energy_divide_by_nspins      1
mc_output_energy_add_readability_lines 1

mc_output_configuration_step     1
mc_output_configuration_archive  0
mc_output_configuration_filetype 3
############### End MC Parameters ################

################ LLG Parameters ##################
### Maximum wall time for single simulation
### hh:mm:ss, where 0:0:0 is infinity
llg_max_walltime        0:0:0

### Seed for Random Number Generator
llg_seed                20006

### Number of iterations
llg_n_iterations        2000000
### Number of iterations after which to save
llg_n_iterations_log    2000

### Temperature [K]
llg_temperature                      0
llg_temperature_gradient_direction   1 0 0
llg_temperature_gradient_inclination 0

### Damping [none]
llg_damping             0.3
### Non-adiabatic damping
llg_beta                0.1

### Time step dt
llg_dt                  1.0E-3

### Bools 0 = false || 1 = true
llg_renorm              1

### 0 = use the pinned monolayer approximation
### 1 = use the gradient approximation
llg_stt_use_gradient        0
### Spin transfer torque parameter proportional
### to injected current density
llg_stt_magnitude           0.0
### Spin current polarisation normal vector
llg_stt_polarisation_normal	1.0 0.0 0.0

### Force convergence parameter
llg_force_convergence   10e-9

### Output configuration
llg_output_any     1
llg_output_initial 1
llg_output_final   1

llg_output_energy_step                  0
llg_output_energy_archive               1
llg_output_energy_spin_resolved         0
llg_output_energy_divide_by_nspins      1
llg_output_energy_add_readability_lines 1

llg_output_configuration_step     1
llg_output_configuration_archive  0
llg_output_configuration_filetype 3
############## End LLG Parameters ################

################ GNEB Parameters #################
### Maximum wall time for single simulation
### hh:mm:ss, where 0:0:0 is infinity
gneb_max_walltime        0:0:0

gneb_spring_constant     1.0

### Bools 0 = false || 1 = true
gneb_renorm              1

### Number of GNEB Energy interpolations
gneb_n_energy_interpolations 10

### Force convergence parameter
gneb_force_convergence   1e-7

### Number of iterations and saves
gneb_n_iterations        200000
gneb_n_iterations_log    200

### Output configuration
gneb_output_any     1
gneb_output_initial 0
gneb_output_final   1

gneb_output_energies_step                  0
gneb_output_energies_interpolated          1
gneb_output_energies_divide_by_nspins      1
gneb_output_energies_add_readability_lines 1

gneb_output_chain_step     0
gneb_output_chain_filetype 3
############## End GNEB Parameters ###############

################ MMF Parameters ##################
### Maximum wall time for single simulation
### hh:mm:ss, where 0:0:0 is infinity
mmf_max_walltime        0:0:0

### Force convergence parameter
mmf_force_convergence   1e-7

### Number of iterations
mmf_n_iterations        200000
### Number of iterations after which to save
mmf_n_iterations_log    200

### Number of modes
mmf_n_modes         10
### Mode to follow
mmf_n_mode_follow   0

### Output configuration
mmf_output_any     1
mmf_output_initial 0
mmf_output_final   1

mmf_output_energy_step                  0
mmf_output_energy_archive               1
mmf_output_energy_divide_by_nspins      1
mmf_output_energy_add_readability_lines 1

mmf_output_configuration_step    0
mmf_output_configuration_archive 1
mmf_output_configuration_filetype 3
############## End MMF Parameters ################



################ EMA Parameters ##################
### Maximum wall time for single simulation
### hh:mm:ss, where 0:0:0 is infinity
ema_max_walltime        0:0:0

### Number of iterations
ema_n_iterations        200000
### Number of iterations after which to save
ema_n_iterations_log    200

### Number of modes
ema_n_modes         10
### Mode to visualize
ema_n_mode_follow   0

### Frequency of displacement
ema_frequency 0.02
### Amplitude of displacement
ema_amplitude 1

### Output configuration
ema_output_any     0
ema_output_initial 0
ema_output_final   0

ema_output_energy_step                  0
ema_output_energy_archive               1
ema_output_energy_divide_by_nspins      1
ema_output_energy_spin_resolved         0
ema_output_energy_add_readability_lines 1

ema_output_configuration_step     0
ema_output_configuration_archive  1
ema_output_configuration_filetype 3
############## End EMA Parameters ################
"""

    out_f = open(output, 'w')
    print(begining_str + coord + end_str, file=out_f)
    out_f.close()

    out_f = open(out_spins, 'w')
    coord=''
    for r in spins:
        for x in r:
            coord += str(x) + ' '
        coord += '\n'
    print(coord, file=out_f)
    out_f.close()
