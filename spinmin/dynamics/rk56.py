import numpy as np
from spinmin.units import R
import dyn_module


class RK56:

    """
    need to integrate this equation

    ds/dt = y ( -[s, b + f' + a [s, b + f']] ) / (M (1 + a^2) )

    s - unit vector
    y - electron gyromagnetic ration
    a - damping parameter
    M = g_i m m_b - length og magnetic moment in m_b (Bohr magneton),
    g_i - Factor Lande
    b - effective field: (- dE/ds)
    f' - random force

    <f' f'> = 2 D' * delta(dt), D' = a kbT M/y

    we will change time step dt to dh =  dt c =dt y / (M (1 + a^2))

    ds = dh (-[s, b + f'(h/c) + a [s, b]])
    <f' f'> = 2 D' delta(h/c) = 2 D' c delta(h) = 2 D delta(h)

    let f(h) = f'(h/c)

    So, we get the equation:

    ds = dh (-[s, b + f + a [s, b + f]])
    <f f> = 2 D delta(h),
    D = T a/(1 + a^2)
    dh = dt R / (m' (1 + a^2)).

    y/M = R / m',
    where R = y/ (m_b),
    ans m' = m g_i -length of magnetic moments (Bohr Magneton)

    Now, if we measure r in (1 / (fs meV))
    and fields in b ((- dE/ds)) in meV,
    then time step is in units of fs

    """

    def __init__(self, eq_type, alpha, dt, temp, damping_only):

        if not damping_only:
            raise ValueError('Can be used for damp LL only')

        self.eq_type = eq_type
        self.temp = temp
        self.damping_only = damping_only
        self.alpha = alpha
        self.dt = dt

        if self.eq_type == 'LLG':
            self.h = dt / (1.0 + alpha**2.0)
        elif self.eq_type == 'LL':
            self.h = dt
        else:
            raise NotImplementedError
        if self.temp is not None:
            self.D = temp * alpha / (1.0 + alpha ** 2)
        else:
            self.D = None

        # R = y/m_b in units of (1 / (fs meV))
        # TODO: we also need to divide it
        #  by a length of magnetic moment
        #  we do it in update right now

        self.h *= R
        self.error = np.inf

        self.a_i = np.asarray([0, 1./5, 3./10, 3./5, 1., 7.8])
        self.c_i = np.asarray([37./378, 0., 250./621, 125./594., 0., 512./1771.])
        self.cp_i = np.asarray([2825./27648, 0., 18575./48384, 13525./55296., 277./14336, 1./4])
        self.b_ij = np.zeros(shape=(6, 5))

        self.b_ij[1][0] = 1.0/5
        self.b_ij[2][0] = 3.0/40
        self.b_ij[3][0] = 3.0/10
        self.b_ij[4][0] = -11.0/54
        self.b_ij[5][0] = 1631.0/55296

        self.b_ij[2][1] = 9.0/40
        self.b_ij[3][1] = -9.0/10
        self.b_ij[4][1] = 5.0/2
        self.b_ij[5][1] = 175.0/512

        self.b_ij[3][2] = 6.0/5
        self.b_ij[4][2] = -70.0/27
        self.b_ij[5][2] = 575.0/13824

        self.b_ij[4][3] = 35.0/27
        self.b_ij[5][3] = 44275.0/110592

        self.b_ij[5][4] = 253.0/4096


    def update(self, ham):

        if ham.type == 'single_image':
            mm = ham.mm[0]
        else:
            mm = 1.0
        D = self.D
        if D is None:
            D = 0.0
            finit_temperature = False
        else:
            finit_temperature = True

        damping_only = self.damping_only
        h, alpha = self.h / mm, self.alpha
        spins_old = ham.spins.copy()
        k = []

        # 6th step
        for j in range(6):
            ham.spins = spins_old.copy()
            for i in range(j):
                ham.spins += self.b_ij[j][i] * k[i]
            b = ham.get_gradients()
            torque = np.cross(ham.spins, b)
            k.append(h * alpha * np.cross(ham.spins, torque))

        spins_star = spins_old.copy()
        ham.spins = spins_old.copy()
        for i in range(6):
            spins_star += self.cp_i[i] * k[i]
            ham.spins += self.c_i[i] * k[i]

        self.Delta = np.linalg.norm(spins_star - ham.spins)
        Delta0 = 1.0e-8
        S = 0.9
        if self.Delta > Delta0:
            self.h = S * self.h * (Delta0/self.Delta)**0.25
        else:
            self.h = S * self.h * (Delta0/self.Delta)**0.2

        self.error = np.max(np.linalg.norm(np.cross(b, ham.spins), axis=1))
